<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 20/08/2018
 * Time: 10:29
 */

namespace App\Security;

use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    private $templating;

    /**
     * UserChecker constructor.
     * @param \Twig_Environment $templating
     * @param FlashBagInterface $flashBag
     */
    public function __construct(\Twig_Environment $templating, FlashBagInterface $flashBag)
    {
        $this->templating   = $templating;
    }

    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface $user
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!$user instanceof AppUser) {
            return;
        }

        if (!$user->isEnabled()) {
            $this->templating->redirectToRoute('form/login.html.twig');
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @param UserInterface $user
     */
    public function checkPostAuth(UserInterface $user)
    {
    }
}
